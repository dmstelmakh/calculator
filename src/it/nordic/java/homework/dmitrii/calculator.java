package it.nordic.java.homework.dmitrii;

import java.util.Scanner;

public class calculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] arr = new int[5];
        for (int i = 1; i <= arr.length; i++) {
            System.out.println("************************************************************");
            System.out.println("Введите режим работы: 1 - тип int, 2 - тип float, 3 - выход.");
            int type = scanner.nextInt();
            if (type == 1) {
                System.out.println("Введите первое число:");
                int number1 = scanner.nextInt();
                System.out.println("Введите второе число:");
                int number2 = scanner.nextInt();
                System.out.println("Введите тип операции: 1 - математичкские операции, 2 - логические операции, 3 - операции сдвига.");
                int operat = scanner.nextInt();
                if (operat == 1) {
                    System.out.println("Введите код операции (1 - сумма, 2 - разность, 3 - произведение, 4 - деление)");
                    int code = scanner.nextInt();
                    int result = 0;
                    int result0 = 0;
                    if (code == 1) {
                        result = number1 + number2;
                    } else if (code == 2) {
                        result = number1 - number2;
                    } else if (code == 3) {
                        result = number1 * number2;
                    } else if (code == 4) {
                        if (number2 != 0) {
                            result = number1 / number2;
                        } else if (number2 == 0) {
                            System.out.println("На ноль делить нельзя!!!");
                            System.out.println("Введите второе число еще раз!");
                            boolean isError = false;
                            int number3;
                            do {
                                number3 = scanner.nextInt();
                                if (number3 != 0) {
                                    isError = false;
                                    result = number1 / number3;
                                } else if (number3 == 0) {
                                    isError = true;
                                    System.out.println("Вы снова ввели ноль. Попробуйте еще раз!");
                                }
                            } while (isError);
                        }
                    }
                    System.out.println("Результат: " + result);

                } else if (operat == 2) {
                    System.out.println("Введите тип логической операции: 1 - первое число = второму числу, 2 - первое число > второго числа, " +
                            "3 - первое число < второго числа");
                    int code2 = scanner.nextInt();
                    boolean result2 = true;
                    if (code2 == 1) {
                        result2 = number1 == number2;
                    } else if (code2 == 2) {
                        result2 = number1 > number2;
                    } else if (code2 == 3) {
                        result2 = number1 < number2;
                    }
                    System.out.println("Результат логичкской операции " + result2);

                } else if (operat == 3) {
                    System.out.println("Введите тип операцции сдвига: 1 - сдвиг >>2, 2 - сдвиг <<2, 3 - сдвиг >>>2.");
                    int code3 = scanner.nextInt();
                    int result31 = 0;
                    int result32 = 0;
                    if (code3 == 1) {
                        result31 = number1 >> 2;
                        result32 = number2 >> 2;
                    } else if (code3 == 2) {
                        result31 = number1 << 2;
                        result32 = number2 << 2;
                    } else if (code3 == 3) {
                        result31 = number1 >>> 2;
                        result32 = number2 >>> 2;
                    }
                    System.out.println("Результат операции сдвига: число 1 = " + result31 + ", число 2 = " + result32);

                }

            } else if (type == 2) {
                System.out.println("Ввeдите первое число:");
                float number1 = scanner.nextFloat();
                System.out.println("Введите второе число:");
                float number2 = scanner.nextFloat();
                System.out.println("Введите код операции (1 - сумма, 2 - разность, 3 - произведение, 4 - деление)");
                float code = scanner.nextFloat();
                float result = 0;
                if (code == 1) {
                    result = number1 + number2;
                } else if (code == 2) {
                    result = number1 - number2;
                } else if (code == 3) {
                    result = number1 * number2;
                } else if (code == 4) {
                    result = number1 / number2;
                }
                System.out.println("Результат: " + result);
            } else
                System.out.println("Вы вышли!");


        }
        System.out.println("Калькулятор перегрелся!");
        System.out.println("*********************************");
        System.out.println(" ");
        System.out.println("Для просмотра результата операции введите ее номер (от 1 до 5)");
        int resPrint = scanner.nextInt();
        if (resPrint == 1)
            System.out.println("Результат 1 = " + arr[0]);
        else if (resPrint == 2)
            System.out.println("Результат 2 = " + arr[1]);
        else if (resPrint == 3)
            System.out.println("Результат 3 = " + arr[2]);
        else if (resPrint == 4)
            System.out.println("Результат 4 = " + arr[3]);
        else if (resPrint == 5)
            System.out.println("Результат 5 = " + arr[4]);
        System.out.println(" ");
        System.out.println("Спасибо что воспользовались калькулятором!");




    }
}
